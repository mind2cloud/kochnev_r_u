#include <vector>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <fstream>
#include <iostream>
#include <algorithm>

using namespace cv;
using namespace std;

double max(double r, double g, double b) {
	if (r > g && r > b)
		return r;
	else if (g > r && g > b)
		return g;
	else return b;
}

double min(double r, double g, double b) {
	if (r < g && r < b)
		return r;
	else if (g < r && g < b)
		return g;
	else return b;
}

Scalar rgbtohsv(double b, double g, double r) {
	double H, S, V;
	double p = max(r, g, b);
	double t = min(r, g, b);

	float delta = p - t;

	if (delta > 0) {
		if (p == r) {
			H = 60 * (fmod(((g - b) / delta), 6));
		}
		else if (p == g) {
			H = 60 * (((b - r) / delta) + 2);
		}
		else if (p == b) {
			H = 60 * (((r - g) / delta) + 4);
		}

		if (p > 0) {
			S = delta / p;
		}
		else {
			S = 0;
		}

		V = p;
	}
	else {
		H = 0;
		S = 0;
		V = p;
	}

	if (H < 0) {
		H = 360 + H;
	}
	return cv::Scalar(H,S,V);
}

vector<cv::Scalar> kmeans_method(Mat data, int number_of_colours) {
	Mat labels, centers;
	cv::kmeans(data, number_of_colours, labels, TermCriteria(cv::TermCriteria::MAX_ITER +
		cv::TermCriteria::EPS, 10, 1.0), 3,
		KMEANS_PP_CENTERS, centers);

	// reshape both to a single row of Vec3f pixels:
	centers = centers.reshape(3, centers.rows);
	data = data.reshape(3, data.rows);

	// replace pixel values with their center value:
	Vec3f* p = data.ptr<Vec3f>();
	vector<cv::Scalar> colours;
	for (size_t i = 0; i < data.rows; i++) {
		int center_id = labels.at<int>(i);
		p[i] = centers.at<Vec3f>(center_id);
		colours.push_back(Scalar(p[i][0], p[i][1], p[i][2]));
	}
	return colours;
}

vector<cv::Scalar> unique_elements(vector<cv::Scalar> colours, int size) {
	Scalar temp = colours[0];
	int count = 1;
	for (int i = 1; i < size * size; i++)
	{
		if (colours[i] != temp)
		{
			for (int j = i + 1; j < size * size; j++)
				if (colours[i] == colours[j])
					colours[j] = temp;
			count++;
		}
	}

	count = 0;
	for (int i = 1; i < size * size; i++)
	{
		if (colours[i] != colours[0])
			colours[++count] = colours[i];
	}
	return colours;
}

vector<cv::Scalar> get_colours(String path, int size, int number_of_colours) {
	Mat src = imread(path);
	Mat ocv;
	resize(src, ocv, Size(size, size));
	// convert to float & reshape to a [3 x W*H] Mat 
	//  (so every pixel is on a row of it's own)
	Mat data;
	ocv.convertTo(data, CV_32F);
	data = data.reshape(1, data.total());

	// do kmeans
	vector<cv::Scalar> colours;
	colours = kmeans_method(data, number_of_colours);

	//<!-----Save only unique colours----!>
	colours = unique_elements(colours, size);

	//<!----Unique colours in BGR---->
	for (int i = 0; i < number_of_colours;  i++)
	{
		cout << colours[i] << endl;
	}

	//<!-----BGR to HSV---->
	for (int i = 0; i < number_of_colours; i++) {
		Scalar c = rgbtohsv(colours[i][0], colours[i][1], colours[i][2]);
		colours[i] = c;
	}
	
	cout << endl;
	//<!----Unique colours in HSV---->
	for (int i = 0; i < number_of_colours; i++)
	{
		cout << colours[i] << endl;
	}

	// back to 2d, and uchar:
	ocv = data.reshape(3, ocv.rows);
	ocv.convertTo(ocv, CV_8U);
	return colours;
}

int main() {
	String path = "./dataset/4/8.jpg";
	vector<cv::Scalar> colours_hsv;
	int number_of_colours = 10;
	int size = 250;
	colours_hsv = get_colours(path, size, number_of_colours);
	for (int i = 0; i < number_of_colours; i++) {
		//green
		if (colours_hsv[i][0] >= 80.0 && colours_hsv[i][0]<=145) {//H
			if (colours_hsv[i][1] * 100.0 >= 40.0) {//S
				if (colours_hsv[i][2] >= 30.0*255.0/100.0) {//V
					cout << "Have green cluster" << endl;
				}
			}
		}
		//blue
		if (colours_hsv[i][0] >= 175.0 && colours_hsv[i][0] <= 195) {//H
			if (colours_hsv[i][1] * 100.0 >= 15.0) {//S
				if (colours_hsv[i][2] >= 90.0 * 255.0 / 100.0) {//V
					cout << "Have blue cluster" << endl;
				}
			}
		}
	}
	    //white
			if (colours_hsv[i][0] >= 175.0 && colours_hsv[i][0] <= 195) {//H
			if (colours_hsv[i][1] * 100.0 >= 15.0) {//S
				if (colours_hsv[i][2] >= 90.0 * 255.0 / 100.0) {//V
					cout << "Have white cluster" << endl;
				}
			}
		}
	}
	    //sunlight
			if (colours_hsv[i][0] >= 175.0 && colours_hsv[i][0] <= 195) {//H
			if (colours_hsv[i][1] * 100.0 >= 15.0) {//S
				if (colours_hsv[i][2] >= 90.0 * 255.0 / 100.0) {//V
					cout << "Have sunlight cluster" << endl;
				}
			}
		}
	}


	waitKey(0);
}